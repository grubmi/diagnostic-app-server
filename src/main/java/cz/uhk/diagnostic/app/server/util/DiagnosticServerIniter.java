package cz.uhk.diagnostic.app.server.util;

import cz.uhk.diagnostic.app.server.repository.MemoryLoadRepo;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;

public class DiagnosticServerIniter extends ChannelInitializer<SocketChannel> {

    private final SslContext sslContext;
    private final MemoryLoadRepo memoryLoadRepo;

    public DiagnosticServerIniter(SslContext sslCtx, MemoryLoadRepo memoryLoadRepo) {
        this.sslContext = sslCtx;
        this.memoryLoadRepo = memoryLoadRepo;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();
        //Order important!!
        pipeline.addLast(sslContext.newHandler(ch.alloc()));
        pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast(new StringDecoder());
        pipeline.addLast(new StringEncoder());
        pipeline.addLast(new DiagnosticServerHandler(memoryLoadRepo));
    }
}
