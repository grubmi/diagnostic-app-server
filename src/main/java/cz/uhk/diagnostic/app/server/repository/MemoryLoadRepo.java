package cz.uhk.diagnostic.app.server.repository;

import cz.uhk.diagnostic.app.server.model.MemoryLoad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MemoryLoadRepo extends JpaRepository<MemoryLoad, Long> {
    @Query("SELECT u FROM MemoryLoad u WHERE u.usedMemory > 0.024")
    List<MemoryLoad> findAllMemBiggerThan();

    MemoryLoad save(MemoryLoad systemLoad);

    List<MemoryLoad> findAll();

    void deleteAll();

    void deleteById(Long id);
}
