package cz.uhk.diagnostic.app.server.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "MemoryLoad")
public class MemoryLoad {

    public MemoryLoad() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Timestamp timestamp;
    private double initMemory;
    private double usedMemory;
    private double maxMemory;
    private double commitMemory;
    private String device;
    private String address;

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public double getInitMemory() {
        return initMemory;
    }

    public void setInitMemory(double initMemory) {
        this.initMemory = initMemory;
    }

    public double getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(double usedMemory) {
        this.usedMemory = usedMemory;
    }

    public double getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(double maxMemory) {
        this.maxMemory = maxMemory;
    }

    public double getCommitMemory() {
        return commitMemory;
    }

    public void setCommitMemory(double commitMemory) {
        this.commitMemory = commitMemory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
