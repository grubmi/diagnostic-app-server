package cz.uhk.diagnostic.app.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiagnosticServerApp {
    public static void main(String[] args) {
        SpringApplication.run(DiagnosticServerApp.class, args);
    }
}
