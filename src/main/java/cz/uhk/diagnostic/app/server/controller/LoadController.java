package cz.uhk.diagnostic.app.server.controller;

import cz.uhk.diagnostic.app.server.model.MemoryLoad;
import cz.uhk.diagnostic.app.server.repository.MemoryLoadRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class LoadController {

    @Autowired
    MemoryLoadRepo memoryLoadRepo;

    @GetMapping(value = {"", "/", "/index"})
    public String index(Model model) {
        List<MemoryLoad> loads = memoryLoadRepo.findAll();
        model.addAttribute("loads", loads);
        return "/index";
    }

    @GetMapping("/value")
    public String value(Model model) {
        List<MemoryLoad> memoryLoads = memoryLoadRepo.findAllMemBiggerThan();
        model.addAttribute("loads", memoryLoads);
        return "/value";
    }

    @GetMapping("/time")
    public String time(Model model) {
        model.addAttribute("loads", showActualTime());
        return "/time";
    }

    @GetMapping("/clear")
    public String clear() {
        memoryLoadRepo.deleteAll();
        return "redirect:/index";
    }

    public List<MemoryLoad> showActualTime() {
        List<MemoryLoad> time = new ArrayList<>();
        List<MemoryLoad> memoryLoads = memoryLoadRepo.findAll();
        for (int i = 0; i < memoryLoads.size(); i++) {
            long now = System.currentTimeMillis();
            long get = memoryLoads.get(i).getTimestamp().getTime();
            if (now - get > 0 && now - get < 12000) { //12 seconds
                time.add(memoryLoads.get(i));
            }
        }
        return time;
    }
}
