package cz.uhk.diagnostic.app.server.service;

import cz.uhk.diagnostic.app.server.repository.MemoryLoadRepo;
import cz.uhk.diagnostic.app.server.util.DiagnosticServerIniter;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service
public class DiagnosticServer {
    private static final int PORT = Integer.parseInt(System.getProperty("port", "8442"));

    //problem with file paths, is better solution?
    private static final String certPath = "C:\\Data\\Java\\diagnostic-app-server\\src\\main\\resources\\cert.crt";
    private static final String keyPath = "C:\\Data\\Java\\diagnostic-app-server\\src\\main\\resources\\key.key";

    @Autowired
    private MemoryLoadRepo memoryLoadRepo;

    //Autostart after spring boot
    @EventListener(ApplicationReadyEvent.class)
    public void initServer() throws IOException, InterruptedException {

        SslContext sslCtx = SslContextBuilder.forServer(
                        new File(certPath),
                        new File(keyPath))
                .build();
        
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new DiagnosticServerIniter(sslCtx, memoryLoadRepo));

            b.bind(getPort()).sync().channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public int getPort() {
        return PORT;
    }
}
