package cz.uhk.diagnostic.app.server.util;

import com.google.gson.Gson;
import cz.uhk.diagnostic.app.server.model.MemoryLoad;
import cz.uhk.diagnostic.app.server.repository.MemoryLoadRepo;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DiagnosticServerHandler extends SimpleChannelInboundHandler<String> {

    private static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private final MemoryLoadRepo memoryLoadRepo;

    public DiagnosticServerHandler(MemoryLoadRepo memoryLoadRepo) {
        this.memoryLoadRepo = memoryLoadRepo;
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        ctx.pipeline().get(SslHandler.class).handshakeFuture().addListener(
                (GenericFutureListener<Future<Channel>>) future -> {
                    ctx.writeAndFlush(
                            "Connected to server \n");
                    channels.add(ctx.channel());
                });
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String msg) throws UnknownHostException {
        for (Channel c : channels) {
            if (c == ctx.channel()) {
                // On "end" close connection
                if (msg.equals("end")) {
                    c.writeAndFlush("Reaction on client signal, connection end " + '\n');
                    ctx.close();
                } else {
                    MemoryLoad load = stringToLoad(msg);
                    load.setDevice(InetAddress.getLocalHost().getHostName());
                    load.setAddress(ctx.channel().remoteAddress().toString());

                    //Save to DB
                    memoryLoadRepo.save(load);
                }
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    public MemoryLoad stringToLoad(String msg) {
        Gson gson = new Gson();
        MemoryLoad memoryLoad = gson.fromJson(msg, MemoryLoad.class);
        return memoryLoad;
    }
}
